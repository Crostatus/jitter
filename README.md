# Jitter  
## A simple network monitoring tool 

**Disclaimer:** *this program is not for professional use. It has been written only for educational pourpose, as an assignment project for the Network Management course at [Unipi](https://di.unipi.it/).*

This is a sniffer example using [libpcap](https://www.tcpdump.org/manpages/pcap.3pcap.html) to analyze the comunication frequency of TCP streams.    
libpcap is a portable C/C++ library for network traffic capture. ([GitHub repository](https://github.com/the-tcpdump-group/libpcap))

**What the jitter measures?** The jitter is defined as the *variation* in the delay (or latency) of received packets.

The aim of this program is to see the jitter of TCP comunications happening from/to the host machine, and detect suspicious jitter variation. 
In this text we will try to cover and explain the essential components of this program and how it works in the following order:    

 1. **Project structure**
 2. **Program usage**
 3. **[WIP] list under construction**
 
## Project structure    
 *Work in progress <3*